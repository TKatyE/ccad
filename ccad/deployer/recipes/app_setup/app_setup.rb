namespace :season do
  
  desc "setup application dependencies on server"
  task :app_setup do
    
    # create the shared/db directory
    
    run "if [ ! -e #{shared_path}/db ]; then mkdir #{shared_path}/db; fi"
    
    # create the shared/config directory
    
    run "if [ ! -e #{shared_path}/config ]; then mkdir #{shared_path}/config; fi"

    # workspace directory (on deploying machine) is a directory deploy_vault/
    # and in there are the deployment-specific files to be pushed to the server
    # they contain mapping data as well as url's - update them before running
    # this cappy task.
    #
    # note that the symlinks for linking these into the release are handled
    # by season:app_symlink - which cannot be run here because the app isn't 
    # deployed yet !
   

    # copy deploy_vault/database.yml -> shared/config/database.yml

      upload("deploy_vault/database.yml", "#{shared_path}/config/database.yml")

    # copy deploy_vault/init_email.rb -> shared/config/init_email.rb

      upload("deploy_vault/init_email.rb", "#{shared_path}/config/init_email.rb")

  end
end