# capistrano recipe : season:app_symlink

namespace :season do

  desc "symlink deployment-specific configs"
  task :app_symlink do

    # symlink shared/config/database.yml -> current_release/config/database.yml

      run "rm -f #{latest_release}/config/database.yml && ln -s #{shared_path}/config/database.yml #{latest_release}/config/database.yml"

    # symlink shared/config/init_email.rb -> current_release/config/initializers/init_email.rb

      run "rm -f #{latest_release}/config/initializers/init_email.rb && ln -s #{shared_path}/config/init_email.rb #{latest_release}/config/initializers/init_email.rb"
      
  end  # :symlink

end # season
