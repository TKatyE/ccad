module UserInterestsHelper
  
  def fields_for_interest(user_interest, &block)
    prefix = user_interest.new_record? ? 'new' : 'existing'
    fields_for("user_interest[#{prefix}_interest_attributes][]", user_interest, &block)
  end
  
  def add_interest_link(name)
    link_to_function name do |page|
      page.insert_html :bottom, :tasks, :partial => 'interest', :object => UserInterest.new
    end
  end
  
end
