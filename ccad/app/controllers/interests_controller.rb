class InterestsController < ApplicationController
  # GET /interests
  # GET /interests.json
  
  
  before_filter :authorize, :only => [:index, :show]
  before_filter :authorize_super, :except => [:index, :show]
  before_filter :user_filter
  
  
  def index
    @interests = Interest.all
    @user = session[:user_id]
    @contact = Contact.find_by_user_id(session[:user_id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @interests }
    end
  end

  # GET /interests/1
  # GET /interests/1.json
  def show
    @interest = Interest.find(params[:id])
    @interest_users = InterestUser.find(:all, :conditions => {:interest_id=>@interest.id})
    logger.info("#{@interest_users}")
    @contact = Contact.find_by_user_id(session[:user_id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @interest }
    end
  end

  # GET /interests/new
  # GET /interests/new.json
  def new
    @interest = Interest.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @interest }
    end
  end

  # GET /interests/1/edit
  def edit
    @interest = Interest.find(params[:id])
  end

  # POST /interests
  # POST /interests.json
  def create
    @interest = Interest.new(params[:interest])

    respond_to do |format|
      if @interest.save
        format.html { redirect_to user_interests_url(@user), notice: 'Interest was successfully created.' }
        format.json { render json: user_interests_url(@user), status: :created, location: @interest }
      else
        format.html { render action: "new" }
        format.json { render json: @interest.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /interests/1
  # PUT /interests/1.json
  def update
    @interest = Interest.find(params[:id])

    respond_to do |format|
      if @interest.update_attributes(params[:interest])
        format.html { redirect_to user_interests_url(@user), notice: 'Interest was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @interest.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /interests/1
  # DELETE /interests/1.json
  def destroy
    @interest = Interest.find(params[:id])
    @interest.destroy

    respond_to do |format|
      format.html { redirect_to user_interests_url(@user) }
      format.json { head :no_content }
    end
  end
  
  protected
  def user_filter
    @user = User.find(session[:user_id])
  end
end
