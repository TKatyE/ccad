class JrResearchesController < ApplicationController
  # GET /jr_researches
  # GET /jr_researches.json
  
  skip_before_filter :authorize, :only => [:new, :create] 
  before_filter :authorize_super, :only => [:new, :create]
  before_filter :user_filter
  
  def index
    @contact = Contact.find_by_user_id(@user_id)
    @jr_researches = JrResearch.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @jr_researches }
    end
  end

  # GET /jr_researches/1
  # GET /jr_researches/1.json
  def show
    @jr_research = JrResearch.find(params[:id])
    @user_interest = InterestUser.find_by_user_id(@jr_research.user_id)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @jr_research }
    end
  end

  # GET /jr_researches/new
  # GET /jr_researches/new.json
  def new
    @jr_research = JrResearch.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @jr_research }
    end
  end

  # GET /jr_researches/1/edit
  def edit
    logger.info("#{@user_id}")
    @contact = Contact.find_by_user_id(session[:user_id])
    @jr_research = JrResearch.find_by_user_id(params[:user_id])
    logger.info("#{@jr_research.inspect}")
    render :action => "edit"
  end

  # POST /jr_researches
  # POST /jr_researches.json
  def create
    @jr_research = JrResearch.new(params[:jr_research])

    respond_to do |format|
      if @jr_research.save
        format.html { redirect_to user_jr_research_url(@user, @jr_research), notice: 'Jr research was successfully created.' }
        format.json { render json: @jr_research, status: :created, location: @jr_research }
      else
        format.html { render action: "new" }
        format.json { render json: @jr_research.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /jr_researches/1
  # PUT /jr_researches/1.json
  def update
    @jr_research = JrResearch.find(params[:id])
    @interest_user = InterestUser.find_by_user_id(session[:user_id])
    @contact = Contact.find_by_user_id(session[:user_id])
    logger.info("#{@interest_user_id}")

    respond_to do |format|
      if @jr_research.update_attributes(params[:jr_research])
        format.html { redirect_to edit_user_interest_user_url(@user, @interest_user), notice: "Your profile has been successfully updated." }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @jr_research.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jr_researches/1
  # DELETE /jr_researches/1.json
  def destroy
    @jr_research = JrResearch.find(params[:id])
    @jr_research.destroy

    respond_to do |format|
      format.html { redirect_to user_jr_researches_url(@user) }
      format.json { head :no_content }
    end
  end
  
  protected
  def user_filter
    @user = User.find(session[:user_id])
  end
end
