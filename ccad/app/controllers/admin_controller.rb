class AdminController < ApplicationController
  
  before_filter :authorize, :only => [:show]
  before_filter :authorize_super, :only => [:index]
  
  def index
    @user_name = User.name
  end
  
  def show 
    @user = User.find(session[:user_id])
    @user_name = @user.name
    @contact = Contact.find_by_user_id(@user)
    @jr_research = JrResearch.find_by_user_id(@user)
  end
  
end
