"""
0|id|INTEGER|1||1
1|user_id|integer|0||0
2|firstname|varchar(255)|0||0
3|lastname|varchar(255)|0||0
4|email|varchar(255)|0||0
5|address_street1|varchar(255)|0||0
6|address_street2|varchar(255)|0||0
7|address_city|varchar(255)|0||0
8|address_state|varchar(255)|0||0
9|address_zip|varchar(255)|0||0
10|picture_url|varchar(255)|0||0
11|created_at|datetime|1||0
12|updated_at|datetime|1||0
13|phone|string|0||0
"""

class Contact < ActiveRecord::Base
  attr_accessible :id, :user_id, :avatar, :firstname, :lastname, :email, :address_street1, :address_street2, :address_city, :address_state, :address_zip, :picture_url, :phone, :title, :phd_md

  belongs_to :user
  
  mount_uploader :avatar, AvatarUploader

   def remove_avatar!
     begin
       super
     rescue Fog::Storage::Rackspace::NotFound
     end
   end

   def remove_previously_stored_avatar
     begin
       super
     rescue Fog::Storage::Rackspace::NotFound
       @previous_model_for_avatar = nil
     end
   end
  
  
end
