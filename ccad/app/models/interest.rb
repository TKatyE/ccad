"""
0|id|INTEGER|1||1
1|area|varchar(255)|0||0
2|created_at|datetime|1||0
3|updated_at|datetime|1||0
"""

class Interest < ActiveRecord::Base
  attr_accessible :id, :area

  has_many :users, :through => :interest_users
  belongs_to :interest_user
  
end
