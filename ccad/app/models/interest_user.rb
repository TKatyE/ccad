"""
0|id|INTEGER|1||1
1|user_id|integer|0||0
2|interest_id|integer|0||0
3|created_at|datetime|1||0
4|updated_at|datetime|1||0
"""

class InterestUser < ActiveRecord::Base
  attr_accessible :id, :user_id, :interest_id
  
  belongs_to :user
  
  has_many :interests
  has_many :users
  
  def update_all(interests, contact)
    
    interest_users = InterestUser.find(:all, :conditions => { :user_id=>contact.user_id })
    logger.info("#{interest_users}")
    
    interest_users.each do |i|
      i.destroy
    end
    
    # For the first entry, update the interest_id from blank to the first.
    interests.each do |i|
      InterestUser.find_or_create_by_user_id_and_interest_id(:user_id=>contact.user_id, :interest_id=>i)
    end  
  end
end
