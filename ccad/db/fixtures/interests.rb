# Read about fixtures at http://api.rubyonrails.org/classes/ActiveRecord/Fixtures.html

# This model initially had no columns defined.  If you add columns to the
# model remove the '{}' from the fixture names and add the columns immediately
# below each fixture, per the syntax in the comments below
#

Interest.seed do |s|
  s.id = 1
  s.area = "Alzheimer's Disease"
end


Interest.seed do |s|
  s.id = 2
  s.area = "miRNA"
end

Interest.seed do |s|
  s.id = 3
  s.area = "Amyloid Plaques"
end


Interest.seed do |s|
  s.id = 4
  s.area = "White Matter"
end


Interest.seed do |s|
  s.id = 5
  s.area = "Grey Matter"
end

Interest.seed do |s|
  s.id = 6
  s.area = "Tau"
end


Interest.seed do |s|
  s.id = 7
  s.area = "Neuroimaging"
end

Interest.seed do |s|
  s.id = 8
  s.area = "Bioinformatics"
end

Interest.seed do |s|
  s.id = 9
  s.area = "apoE"
end

Interest.seed do |s|
  s.id = 10
  s.area = "Traumatic Brain Injury"
end

Interest.seed do |s|
  s.id = 11
  s.area = "Biomarkers"
end

Interest.seed do |s|
  s.id = 12
  s.area = "AB"
end

Interest.seed do |s|
  s.id = 13
  s.area = "Brain Injury"
end

Interest.seed do |s|
  s.id = 14
  s.area = "Genetics"
end

Interest.seed do |s|
  s.id = 15
  s.area = "Gene Expression"
end

Interest.seed do |s|
  s.id = 16
  s.area = "MRI"
end

Interest.seed do |s|
  s.id = 17
  s.area = "Biophysics"
end

Interest.seed do |s|
  s.id = 18
  s.area = "Diffusion Kertosis Imaging (DKI)"
end



Interest.seed do |s|
  s.id = 19
  s.area = "Stroke"
end

Interest.seed do |s|
  s.id = 20
  s.area = "Biochemistry"
end

Interest.seed do |s|
  s.id = 21
  s.area = "Synaptic Transmission"
end

Interest.seed do |s|
  s.id = 22
  s.area = "Epigenetics"
end

Interest.seed do |s|
  s.id = 23
  s.area = "Mouse/Animal Models"
end

Interest.seed do |s|
  s.id = 24
  s.area = "Nitric Oxide"
end

Interest.seed do |s|
  s.id = 25
  s.area = "Signalling Pathways"
end




