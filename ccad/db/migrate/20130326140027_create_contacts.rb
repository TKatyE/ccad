class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.integer :user_id
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :address_street1
      t.string :address_street2
      t.string :address_city
      t.string :address_state
      t.string :address_zip
      t.string :picture_url
      t.timestamps
    end
  end
end
