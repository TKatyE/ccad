require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end

  test "should login" do
    tara = users(:one)
    post :create, name: tara.name, password: "secret"
    assert_redirected_to admin_ur
    assert_equal tara.id, session[:user_id]
    assert_response :success
  end

  test "should fail login" do
    tara = users(:one)
    post :create, name: tara.name, password: "wrong"
    assert_redirected_to login_url
  end

  test "should logout" do
    delete :destroy
    assert_redirected_to store_url
  end
end
