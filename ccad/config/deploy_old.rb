# Capistrano Deployment File for driftergifter.com
#

require 'bundler/capistrano'

# source_root is where the source code tree is on deploying machine

set :source_root,   "/Users/tke/Projects/ccad/ccad"
# load the capistrano recipes defined for this rails project


# load "#{source_root}/deployer/recipes/base.rb"
load "#{source_root}/deployer/recipes/app_symlink/app_symlink.rb"
load "#{source_root}/deployer/recipes/app_setup/app_setup.rb"


# set the deployment attributes for this venue/application
server "driftergifter.com", :app, :web, :db, primary: true

set :application,   "ccad"

set :rails_env,     "production"
set :port,          55232

set :scm,           "git"
set :repository,    "https://TKatyE@bitbucket.org/TKatyE/ccad.git"
set :user,          "TKatyE"
set :password,      "ratS9a%rat"
set :branch,        "master"
set :deploy_via,    "remote_cache"

set :deploy_to,     "/home/driftergifter.com/apps/driftergifter"
set :keep_releases, 5

set :user,          "driftergifter.com"
set :use_sudo,      false

set :rake,          "bundle exec rake"

default_run_options[:pty] = true

before "deploy:assets:precompile",    "season:app_symlink"
after  "deploy",                      "deploy:cleanup"

# add in the apache/passenger restart task

namespace :deploy do
 task :start do ; end
 task :stop do ; end
 task :restart, :roles => :app, :except => { :no_release => true } do
   run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
 end
end



