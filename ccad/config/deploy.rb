set :application, "driftergifter"
set :rails_env,     "production"
set :port,          55232

set :scm,           "git"
set :repository,  "https://TKatyE@bitbucket.org/TKatyE/ccad.git"
set :branch,        "master"
set :deploy_via,    "copy"

# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

server "driftergifter.com", :app, :web, :db, primary: true

set :user,          "driftergifter.com"
set :deploy_to,     "/home/driftergifter.com/apps/driftergifter"
set :keep_releases, 5



set :rake,          "bundle exec rake"

default_run_options[:pty] = true


before "deploy:assets:precompile",    "season:app_symlink"
after  "deploy",                      "deploy:cleanup"



namespace :deploy do
 task :start do ; end
 task :stop do ; end
 task :restart, :roles => :app, :except => { :no_release => true } do
   run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
 end
end


# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end