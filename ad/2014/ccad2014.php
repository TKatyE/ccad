<?php 
$ini_array = parse_ini_file("config.ini");
$root_url = $ini_array["root_url"];

include $root_url . 'head2014.html';
include $root_url . 'navbar.html';
?>

<div class="top_photo">
  <img src=<?php echo $root_url . "/images/tablelong.jpg"?>>
</div>

<body>
  <div class="container-fluid main_page">
      <div class="row-fluid">
        <div class="span12">
          <h1 class="center header_main" style="margin-bottom: 10px;">Charleston Conference on Alzheimer's Disease 2014</h1>
        </div>
      </div>
  </div>
  
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <h4 class="center">After the success of last year's inaugural CCAD conference, plans for CCAD 2014 are underway.</h4>
        <h5 class="center">CCAD 2014 will be held in Charleston, South Carolina on February 28 - March 2, 2014.</h5>
        <div class="divider_red"></div>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span12">
        <div class="span6" style="margin-top: 50px;">
          <h4>Conference Format<h4>
          <p>Following the model of <a href="/ad/2013/ccad2013.php">CCAD 2013</a>, the CCAD 2014 conference participants will include five Nominators and 12 Junior Investigators (JI).  In the months before the conference, each JI will prepare a grant application for how they would use $50,000 of funding to engage in research that is novel and high-risk or high-impact.  JI's are encourged to collaborate with their elected colleagues on proposals, but are also welcome to work alone.</p>
          <p>During the conference, the JI's will present their proposals and then team up in small groups led by each of the Nominators where they will mark each others' proposals.  After this study session, the 'Council' (i.e. the Mentors and Funders) will select four investigators/proposals for funding.</p>
          <p>While the monetary award is impressive on its own, more imporantly, over the course of the weekend in Charleston, the JI's will have the opportunity to meet and mingle both formally and socially with their fellow peers, allowing them to learn both about each others' research and outside interests.  With the small size of the conference and diversity of specific interests (but shared overall interest) of the group, the JI's will have ample time to be in the spotlight and begin to form a cohort with their peers.  As the future generation of leaders in the Alzheimer's disease research field, the JI's of CCAD 2014, along with the JI's of CCAD 2013 and future participants, will be able to establish crucial relationships with their colleagues to broaden collaborative efforts in AD research.</p> 
          <div class="divider_red"></div>  


          <h4>Conference Location</h4>
          <h5>Charleston, South Carolina</h5>
          <p>
            <img src=<?php echo $root_url . "/images/Charleston_NOB.jpg"?> style="float: right; width: 260px; height: auto; padding: 20px;">
            CCAD 2014 will be held in Charleston, South Carolina.  Voted for the second year in a row as the 'Top City in the United States' by the Conde Nast Traveler Reader's Choice Awards, Charleston boast historical charm, world-class restaurants and the beach on your doorstep.  There will be an afternoon break on Day 2 of the conference for sightseeing around the city.  For more information about what Charleston has to offer, check out the links below.
          </p>
            <p class="center"><a href="http://www.charlestonly.com/">Charlestonly.com</a> | <a href="http://www.charlestoncvb.com/visitors/">Charleston Vistors Bureau</a> | <a href="http://www.discovercharleston.com/index.htm">Discover Charleston</a></p>
          <div class="divider_red"></div> 
          <br />
          <h4>Travel</h4>
          
          <p>Conference-related expenses, such as room reservations and meals, are already covered. For transportation expenses, each Junior Investigator will be provided with a stipend in the amount of $600. For tax purposes, each Junior Investigator will have to provide his/her full name, home address, and social security number upon registering at the Conference, to receive this stipend.</p>
          <div class="divider_red"></div>    
        </div>
        
        <div class="span6">
        <div class="right_tan" style="margin-top: 50px;">
          <h2 style="margin-bottom: 30px;">Junior Investigator Criteria</h2>
          
          <div class="divider_red"></div>
          
          <p>Junior Investigators (JI's) will be nominated by either one of the Nominators or one of the CCAD 2013 Awardees and will meet the following criteria:</p>
          <ul>
            <li class="bullets">Must have a PhD and/or MD degree, and not hold an academic rank higher than Assistant Professor, and not be beyond 10 years fo the receipt fo their degree or the completion of their clinical or post-doctoral training.</li>
            <li class="bullets">Must hold the aforementioned position in a U.S. institution, in which the proposed research will be conducted.</li>
            <li class="bullets">Only one JI can be from a Nominator's lab/department center.</li>
            <li class="bullets">Have demonstrated a commitment to a career in advancing AD-related research, as evidence by their prior training and research productivity.</li>
            <li class="bullets">Have contributed to current AD-related research to the degree that is appropriate for a researcher of JI status, but demonstrates promise for greater prodcutivity and innovation.</li>
            <li class="bullets">Have the assets and qualitites necessary to flourish in multi-disciplinary, collaborative research endeavors.</li>
          </ul>
          <div class="divider_red"></div>
  
          <p>At this time, we are not accepting applications from Junior Investigators.  However, we may open up the CCAD conference opportunity to interested and qualified JI's in the future.</p>
  
          </div>
          <h4 style="padding-top: 20px;">Venue</h4>
          <h5>Charleston Place Hotel</h5>
          <p><img src=<?php echo $root_url . "/images/chsplace.jpg"?> style="width: 260px; height: auto; float: left; padding: 20px;"></p>

          <p style="margin-top: 60px;">205 Meeting Street<br />Charleston, SC 29401</p>
          <p><a href="http://www.charlestonplace.com/">http://www.charlestonplace.com/</a></p>
  
          <div class="divider_red" style="margin-top: 60px;"></div>  
        </div>
      </div>
      </div>
    </div>
  </div>
</body>
    

