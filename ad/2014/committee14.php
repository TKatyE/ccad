<?php 
$ini_array = parse_ini_file("config.ini");
$root_url = $ini_array["root_url"];

include $root_url . 'head2014.html';
include $root_url . 'navbar.html';
?>

<img src=<?php echo $root_url . "/images/nominatorsbanner.png"?> class="top_photo">
<body>
  <div class="container-fluid main_page">
      <div class="row-fluid">
        <div class="span12">
          <h1 class="center header_main">Charleston Conference on Alzheimer's Disease 2014</h1>
          <h2 class="center">Scientific Board (The Mentors)</h2>

          <div class="divider_red" style="margin-top: 20px; margin-bottom: 10px;"></div>
      
          <p class="desc_text" style="margin-bottom: 20px;">The CCAD 2014 Mentors will again be comprised of top-level, world-renown experts in Alzheimer's disease research with a wide range of specialties.  The Mentors will serve as the 'Scientific Board' during the grant application review process.  Throughout the CCAD conference, the Junior Investigators will have both structured and informal time to meet with the Mentors.</p>
          <div class="divider_red" style="margin-top: 20px; margin-bottom: 10px;"></div>
        </div>
      </div>
    </div>
  
  <div class="container">
    <div class="bio_text">
      <img src=<?php echo $root_url . "/images/joehead.png"?> class="bio_photo">
      <h3><a href="http://academicdepartments.musc.edu/cbi/people/helpern_profile">Joseph A. Helpern, Ph.D.</a></h3>
      <h5>Professor, Departments of Radiology and Neuroscience, Director, Center for Biomedical Imaging, Medical University of South Carolina</h5>
      <p>Dr. Helpern is a professor of Radiology and Neuroscience and head of the Center for Biomedical Imaging at the Medical University of South Carolina.  Dr. Helpern has made numerous, significant contributions to the field of magnetic resonance imaging (MRI).  His current research interests focus on using diffusional kurtosis imaging (DKI), a refinement of diffusion tensor imaging (DTI) developed by him and long-time colleague Jen Jensen, Ph.D., to identify changes in the brain microarchitecture that could be predictive of the early stages of Alzheimer's disease, additional areas of involvement in stroke and brain tissue patterns in children with ADHD.  Dr. Helpern is the CCAD chair.</p>
    </div>
    <div class="divider_red" style="margin-top: 20px; margin-bottom: 10px;"></div>
    
    
    <div class="bio_text">
      <img src=<?php echo $root_url . "/images/gbar.jpg"?> class="bio_photo">
      <h3><a href="http://faculty.bri.ucla.edu/institution/personnel?personnel_id=8435">George Bartzokis, M.D.</a></h3>
      <h5>Professor, Department of Psychiatry, UCLA Neuropsychiatric Hospital and Semel Institute, David Geffen School of Medicine at UCLA</h5>
      <p>Dr. Bartzokis is a professor of Neurology at the UCLA School of Medicine and is the director of the UCLA Memory Disorders and Alzheimer’s Disease Clinic and the Clinical Core Director at the Alzheimer Disease Research Center.  After earning his medical degree from Yale University School of Medicine, Dr. Bartzokis completed a Schizophrenia Research Fellowship at the UCLA Neuropsychiatric Hospital.  Clinically, Dr. Bartzokis’s medical interests focus on Alzheimer’s disease and memory disorders.</p>
    </div>
    <div class="divider_red" style="margin-top: 20px; margin-bottom: 10px;"></div>
    
    <div class="bio_text">
      <img src=<?php echo $root_url . "/images/Davies.jpg"?> class="bio_photo">
      <h3><a href="http://www.einstein.yu.edu/faculty/3146/peter-davies/">Peter Davies, Ph.D.</a></h3>
      <h5>Scientific Director, Litwin-Zucker Center for the Study of Alzheimer's Disease and Memory Disorders, Feinstein Institute for Medical Research, Albert Einstein College fo Medicine of Yeshiva University</h5>
      <p>Dr. Davies’s research focuses on the mechanisms important for the development of Alzheimer’s disease using primarily human brain tissues.  After earning his PhD in Biochemistry from the University of Leeds, he began his work on Alzheimer’s disease at the Medical Research Council Brain Metabolism Unit in Edinburgh before moving to the Albert Einstein College of Medicine in New York.  Since 2005, Dr. Davies has been the Head and Director of the Litwin-Zucker Research Center for the Study of Alzheimer’s Disease and Memory Disorders, associated with the Feinstein Institute for Medical Research.  </p>
    </div>
    <div class="divider_red" style="margin-top: 20px; margin-bottom: 10px;"></div>
    
    <div class="bio_text">
      <img src=<?php echo $root_url . "/images/Duff.jpg"?> class="bio_photo">
      <h3><a href="http://asp.cumc.columbia.edu/facdb/profile_list.asp?uni=ked2115&DepAffil=Psychiatry">Karen Duff, Ph.D.</a></h3>
      <h5>Professor, Departments of Pathology and Cell Biology, Taub Institute for Alzheimer's Disease Research, Columbia University Medical Center</h5>
      <p>Dr. Duff is known for her research on Alzheimer’s disease using innovative methods in developing transgenic mouse models for age-related disorders.  After completing her PhD at the University of Cambridge, Dr. Duff worked at the University of London before moving to the University of South Florida.  Dr. Duff then moved to the Nathan Kline Institute for Psychiatric Research and later to the Taub Institute for Alzheimer’s Disease research at Columbia University Medical Center.</p>
    </div>
    <div class="divider_red" style="margin-top: 20px; margin-bottom: 10px;"></div>
    
    <div class="bio_text">
      <img src=<?php echo $root_url . "/images/Goate.jpg"?> class="bio_photo">
      <h3><a href="http://dbbs.wustl.edu/faculty/Pages/faculty_bio.aspx?SID=1565">Alison Goate, D.Phil</a></h3>
      <h5>Professor, Departments of Psychiatry, Genetics and Neurology, Director, Hope Center for Neurological Disorders, Washington Universtiy School of Medicine</h5>
      <p>Dr. Goate’s research focuses on the molecular genetics of Alzheimer’s disease and substance dependence using gene sequence strategies.  After completing her D.Phil at the University of Oxford, Dr. Goate continued her post-doctoral studies at St Mary’s Hospital Medical School in London before moving to Washington University as an Associate Professor in Genetics and Psychiatry.  Over the course of her career, Dr. Goate and her colleagues have identified mutations for the amyloid precursor protein along with four other gene mutations involved in Alzheimer’s disease and dementia.</p>
    </div>
    <div class="divider_red" style="margin-top: 20px; margin-bottom: 10px;"></div>
    
    <div class="bio_text">
      <img src=<?php echo $root_url . "/images/wolozin.jpg"?> class="bio_photo">
      <h3><a href="http://www.bu.edu/alzresearch/team/faculty/wolozin/">Benjamin Wolozin, M.D., Ph.D.</a></h3>
      <h5>Professor, Departments of Pharmacology and Neurology, Boston University School of Medicine</h5>
      <p>After earning his M.D. and Ph.D. degrees from Albert Einstein College of Medicine, Dr. Wolozin worked at Mt. Sinai Medical Center and the National Institute of Mental Health before joing Loyola Univeristy Medical Center.  In 2004, Dr. Wolozin moved to Boston University School of Medicine as a professor.  Dr. Wolozin’s research focuses on the role of cholesterol in the pathophysiology of Alzheimer’s disease.</p>
    </div>
    <div class="divider_red" style="margin-top: 20px; margin-bottom: 10px;"></div>
    
    
