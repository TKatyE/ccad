<?php 
$ini_array = parse_ini_file("config.ini");
$root_url = $ini_array["root_url"];

include $root_url . 'head2013.html';
include $root_url . 'navbar.html';
?>

<body>
  <img src=<?php echo $root_url . "/images/frontiersbanner.png"?> class="top_photo">
  <div class="container-fluid main_page">
      <div class="row-fluid">
        <div class="span12">
          <h1 class="center header_main">Frontiers of Alzheimer's Disease Research</h1>
          <h3 class="center" style="margin-top: -60px;">Thursday, February 28, 2013</h3>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span12">
          <div class="divider_red"></div>
    
          <h4 class="center"><a href="/images/fadprogram.pdf">Frontiers of Alzheimer's Disease Research 2013 Program</a></h4>
          <div class="divider_red" style="margin-top: 30px;"></div>
        </div>
      </div>
    </div>
    
    <div class="container-fluid">
  
      <div class="row-fluid">
        <div class="span12">
          <div class="span5">
            <p class="main_text" style="margin-top: 80px;">Along with the Charleston Conference on Alzheimer's disease (<a href=<?php echo $root_url ."/ad/2013/cadd2013.php"?>>CCAD</a>), the  "Frontiers of Alzheimer's disease Research" was also introduced.  During the afternoon on the Thursday before the CCAD weekend, renowned AD researchers were invited to present their recent work to the Medical University of South Carolina (MUSC) community.  The researchers then participated in the CCAD conference as the mentors/nomination committee.</p>

            <img src=<?php echo $root_url . "/images/circles.png"?> style="width: 40px; height: auto; margin-bottom: 60px; margin-left: 44%;">

            <p>This symposium was made possible by a generous philanthropic gift to MUSC to establish the CCAD, and is presented with the support of the MUSC Center on Aging and the Alzheimer's Research and Clinical Programs.</p>
          </div>
          
          <div class="span7">
            <div class="right_tan">
 
              <h3 class="center">Frontiers of AD Research 2013 Talks and Speakers:</h3>
              <p><strong>"Tau immunotherapy in transgenic mice"</strong></p>
              <p>Peter Davies, PhD; Scientific Director, Litwin-Zucker Center for the Study of Alzheimer's Disease and Memory Disorders, Feinstein Institute for Medical Research, Albert Einstein College of Medicine of Yeshiva University</p>

              <p><strong>"Propagation of Alzheimer's related tauopathy in vivo and in vitro"</strong></p>
              <p>Karen Duff, PhD; Professor, Departments of Pathology and Cell Biology, Taub Institute for Alzheimer's Disease Research, Columbia University Medical Center</p>

              <p><strong>"Use of endophenotypes in the genetic analysis of late onset Alzheimer's disease"</strong></p>
              <p>Alison Goate, D.Phil; Professor, Departmetns of Psychiatry, Genetics and Neurology, Director, Hope Center for Neurological Disorders, Washington University School of Medicine</p>

              <p><strong>"Neurodegeneration beyond the tangle"</strong></p>
              <p>Benjamin Wolozin, MD, PhD; Professor, Departments fo Pharmacology and Neurology, Boston University School of Medicine</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>