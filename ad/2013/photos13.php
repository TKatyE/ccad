<?php 
$ini_array = parse_ini_file("config.ini");
$root_url = $ini_array["root_url"];

include $root_url . 'head2013.html';
include $root_url . 'navbar.html';
?>

<div class="container-fluid main_page">
  <div class="row-fluid">
    <div class="span12">
      <div class="span6">
        <h1 class="center header_margin_large">CCAD 2013 Photos</h1>
        <h4 class="center small_header">2013 Schedule</h4>
        <h3 class="center small_header">Day 1: Thursday, February 28, 2013</h3>
        <p class="center">Arrive in Charleston<br />Reception at the Palmetto Garden Room</p>
        <h3 class="center small_header">Day 2: Friday, March 1, 2013</h3>
        <p class="center">JI's Present Grant Applications in The Pavilion Room</p>
        <h3 class="center small_header">Day 3: Saturday, March 2, 2013</h3>
        <p class="center">Review of Grant Applications (morning)<br />Sightseeing (afternoon)<br />Dinner Banquet and Awards Ceremony (evening)</p>
        <h3 class="center small_header">Day 4: Sunday, March 3, 2013</h3>
        <p class="center">Attendees Depart</p>
    
      </div>
      <div class="span6">

        <div id="slides">
          <img src=<?php echo $root_url . "/images/135.jpg"?>>
          <img src=<?php echo $root_url . "/images/136.jpg"?>>
          <img src=<?php echo $root_url . "/images/139.jpg"?>>
          <img src=<?php echo $root_url . "/images/141.jpg"?>>
          <img src=<?php echo $root_url . "/images/142.jpg"?>>
          <img src=<?php echo $root_url . "/images/145.jpg"?>>
          <img src=<?php echo $root_url . "/images/168.jpg"?>>
          <img src=<?php echo $root_url . "/images/178.jpg"?>>
          <img src=<?php echo $root_url . "/images/186.jpg"?>>
          <img src=<?php echo $root_url . "/images/193.jpg"?>>
          <img src=<?php echo $root_url . "/images/helpern_friday_sm.jpg"?>>
          <img src=<?php echo $root_url . "/images/data_sm.jpg"?>>
          <img src=<?php echo $root_url . "/images/colleen_heather_anya_joe_sm.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01677.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01691.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01717.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01723.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01724.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01729.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01733.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01736.jpg"?>>
          <img src=<?php echo $root_url . "/images/group1_sm.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01738.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01741.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01743.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01745.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01747.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01748.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01749.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01750.jpg"?>>
          <img src=<?php echo $root_url . "/images/DSC01751.jpg"?>>
          <img src=<?php echo $root_url . "/images/juniors_w_pic_sm.jpg"?>>     

          <a href="#" class="slidesjs-navigation"><i class="icon-large"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>