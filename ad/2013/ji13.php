<?php 
$ini_array = parse_ini_file("config.ini");
$root_url = $ini_array["root_url"];

include $root_url . 'head2013.html';
include $root_url . 'navbar.html';
?>

<img src=<?php echo $root_url . "/images/investbanner.png"?> class="top_photo">
<body>
  <div class="container-fluid main_page">
      <div class="row-fluid">
        <div class="span12">
          <h1 class="center header_main">Charleston Conference on Alzheimer's Disease 2013</h1>
          <h2 class="center" style="margin-top: -40px;">Junior Investigators</h2>
        </div>
      </div>
  </div>
  
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="divider_red" style="margin-top: 20px; margin-bottom: 10px;"></div>
        
        <p class="desc_text">The Junior Investigators for CCAD 2013 were selected by the <a href="committee13.php">nomination committee</a> and represent mid-level researchers from a range of specialties in the Alzheimer's disease field from institutions across the United States.  Before the conference, each researcher prepared a proposal for how they would use $50,000 worth of funding for high-risk and potentially high-impact research.  Before the conference, the participants were provided with the information of their peers and encouraged to collaborate with them to develop proposals to extend their own work to make use of each others' interests and specialities.  Throughout the conference weekend, the JI's were able to get to know each other both scientifically and socially, and many walked away with ideas for collaborations and friends in the field!</p>
        
        <div class="divider_red" style="margin-top: 30px;"></div>
        
        </div>
      </div>
    </div>
  </div>
  
  <div class="container">
    <div class="row">
      <div class="span12 bio">
        <ul class="thumbnails">
          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/Brickmanhead.jpg"?> class="pic">
              <h4>Adam Brickman, PhD</h4>
              <p class="small_text">Taub Institute for Research on Alzheimer's Disease and the Aging Brain</p>
              <p class="small_text">Columbia University</p>
            </div>
          </li>
          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/burns.jpg"?> class="pic">
              <h4>Mark Burns, PhD</h4>
              <p class="small_text">Department of Neuroscience</p>
              <p class="small_text">Georgetown University Medical Center</p>
            </div>
          </li>
          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/Engelman_Corinne_001.jpg"?> class="pic">
              <h4>Corinne D. Engelman, PhD</h4>
              <p class="small_text">Department of Population Health Sciences</p>
              <p class="small_text">University of Wisconsin School of Medicine and Public Health</p>
            </div>
          </li>
          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/Fieremans_Els7030c.jpg"?> class="pic">
              <h4>Els Fieremans, PhD</h4>
              <p class="small_text">Center for Biomedical Imaging</p>
              <p class="small_text">New York University Medical Center</p>
            </div>
          </li>
          </ul>
        
        </div>
        
        <div class="span12">
          <ul class="thumbnails">
          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/Francis.jpg"?> class="pic">
              <h4>Yitshak Francis, PhD</h4>
              <p class="small_text">Taub Institute for Research on Alzheimer's Disease and the Aging Brain</p>
              <p class="small_text">Columbia University</p>
            </div>
          </li>

        <li class="span3">
          <div class="thumbnail">
            <img src=<?php echo $root_url . "/images/GoveasJoseph.jpg"?> class="pic">
            <h4>Joseph S. Goveas, MD</h4>
            <p class="small_text">Department of Psychiatry and Behavioral Medicine</p>
            <p class="small_text">Medical College of Wisconsin</p>
          </div>
        </li>
        <li class="span3">
          <div class="thumbnail">
            <img src=<?php echo $root_url . "/images/kauwe.jpg"?> class="pic">
            <h4>John S. K. Kauwe, PhD</h4>
            <p class="small_text">Department of Biology</p>
            <p class="small_text">Brigham Young University</p>
          </div>
        </li>
        <li class="span3">
          <div class="thumbnail">
            <img src=<?php echo $root_url . "/images/Kim.jpg"?> class="pic">
            <h4>Jungsu Kim, PhD</h4>
            <p class="small_text">Department of Neurology</p>
            <p class="small_text">Washington University School of Medicine</p>
          </div>
        </li>
        </ul>
        </div>
        <div class="span12">
          <ul class="thumbnails">
          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/Kitazawa.jpg"?> class="pic">
              <h4>Masashi Kitazawa, PhD</h4>
              <p class="small_text">Molecular and Cell Biology</p>
              <p class="small_text">University of California, Merced</p>
            </div>
          </li>

          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/meadowcroft.jpg"?> class="pic">
              <h4>Mark D. Meadowcroft, PhD</h4>
              <p class="small_text">Department of Radiology</p>
              <p class="small_text">Pennsylvania State University School of Medicine</p>
            </div>
          </li>
          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/Nakamura.jpg"?> class="pic">
              <h4>Tomohiro Nakamura, PhD</h4>
              <p class="small_text">Center of Neuroscience, Aging and Stem Cell Research</p>
              <p class="small_text">Sanford-Burnham Medical Research Institute</p>
            </div>
          </li>
          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/oddo.jpg"?> class="pic">
              <h4>Salvatore Oddo, PhD</h4>
              <p class="small_text">Department of Physiology</p>
              <p class="small_text">University of Texas Health Science Center at San Antonio</p>
            </div>
          </li>
        </ul>
      </div>
        
      <div class="span9" style="margin-left: 120px;">
        <ul class="thumbnails">
          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/rissman.jpg"?> class="pic">
              <h4>Robert A. Rissman, PhD</h4>
              <p class="small_text">Department of Neurosciences</p>
              <p class="small_text">University of California, San Diego, School of Medicine</p>
            </div>
          </li>

          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/Schon_Photo_cropped_080712.jpg"?> class="pic">
              <h4>Karin Schon, PhD</h4>
              <p class="small_text">Center for Memory and Brain</p>
              <p class="small_text">Boston University</p>
            </div>
          </li>
          <li class="span3">
            <div class="thumbnail">
              <img src=<?php echo $root_url . "/images/zimmerman.jpg"?> class="pic">
              <h4>Molly E. Zimmerman, PhD</h4>
              <p class="small_text">Saul R. Korey Department of Neurology</p>
              <p class="small_text">Albert Einstein College of Medicine</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
    
</body>
</html>