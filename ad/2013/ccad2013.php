<?php 

$ini_array = parse_ini_file("config.ini");
$root_url = $ini_array["root_url"];

include $root_url . 'head2013.html';
include $root_url . 'navbar.html';

?>

<div class="top_photo">
  <img src=<?php echo $root_url . "/images/tables.jpg"?>>
</div>
<body>
    <div class="container-fluid main_page">
        <div class="row-fluid">
          <div class="span12">
            <h1 class="center header_main">Charleston Conference on Alzheimer's Disease 2013</h1>
            <h5 style="margin-top: -50px; margin-bottom: 40px;">To find out more about the conference, you can read the blog post here.</h5>
          </div>
        </div>
    </div>
    
    
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span12">
          <div class="span6">
            <h4 class="center">How Did CCAD 2013 Work?</h4>
            
            <div class="divider_red"></div>
            
            <h5>Conference Format</h5>
            <p class="main_text">The Charleston Conference on Alzheimer’s Disease (CCAD) series was initiated in Spring 2013 at the request and with the support of philanthropists Leonard Litwin and Carole L. Pittelman.  With a personal connection to Alzheimer’s disease (AD), Leonard and Carole feel strongly that the current time-to-bench and stringent nature of grant and research funding is stifling researchers from working on high-risk or high-impact ideas in favor of those that are less adventurous, more low-risk and therefore a better investment of their grant writing time and energies.  The CCAD conference series aims to take away these restraints and allow a set of <a href="ji13.php">‘Junior Investigators’</a> (JI’s, i.e. strong researchers at the Post-Doctoral or Assistant Professor level) to propose novel ideas in the area of AD research.</p>  
            <h5>Pre-Conference</h5>
            <p class="main_text">The JI team was selected by a team of <a href="committee13.php">five Mentors</a> made up of high-profile researchers in the AD field based on their research, its relevance to enhancing the current understanding of the pathophysiology and treatment of AD and the representativeness of their subfields in current AD research initiatives.  Over the six months preceding the conference, the JI’s were tasked with writing a proposal in the format of an NIH grant application for how they would use $50,000 of funding to further AD research.  Before writing their proposals, the JI’s were given the contact information for their colleagues participating in the conference, with the option and encouragement to team up and collaborate with the other JI’s to expand their ideas.</p>
            <h5>During the Conference</h5>
            <p class="main_text">During the conference itself, each JI put forth their proposal in a 15-minute oral presentation and answered questions from the Nominators and others present.  
              <a href=<?php echo $root_url . "/ad/2013/photos13.php"?>><img src=<?php echo $root_url . "/images/photobtn.png"?>  style="float: left; padding-right: 20px;" class="link_btn"></a>
              Following the presentations, the JI’s were split into five break-out groups led by each of the Mentors, where they scored applications within the Mentor’s field of study.  Once all of the applications were scored, the proposals were sent to “Council” with the Mentors, Dr. Joseph Helpern (Conference Chair), Charlie Dorego and a member of the planning committee, to select the proposals that would receive the funding.</p>
            <p class="main_text">The “New Vision Award” was presented to four of the JI’s along with $50,000 to fund the proposal.  Check out the awardees of the 2013 CCAD Conference at right!</p>

            <img src=<?php echo $root_url . "/images/learningquote.png"?> class="quotes"></img>
            <a href="http://sandyseas.com/test32819/forum3412/"><img src=<?php echo $root_url . "/images/forumcircle.png"?> class="more_button" style="margin-left: 20%;"></a>
            <a href=<?php echo $root_url . "/blog"?>><img src=<?php echo $root_url . "/images/blog.png"?> class="more_button"></a>
        </div>
        
        
        <div class="span6" style="margin-top: 40px;">      
          <div class="right_tan">
            <h2 class="center red">2013 New Vision Awardees</h2>
            <ul class="nav nav-pills" style="margin-top: 40px; margin-left: 12%">
              <li class="active"><a href="#oddo" data-toggle="tab"><img src=<?php echo $root_url . "/images/oddo.jpg"?>  class="bio_pic"></a></li>
              <li><a href="#meadow" data-toggle="tab"><img src=<?php echo $root_url . "/images/meadowcroft.jpg"?>  class="bio_pic"></a></li>
              <li><a href="#kim" data-toggle="tab"><img src=<?php echo $root_url . "/images/Kim.jpg"?> class="bio_pic"></a></li>
              <li><a href="#kauwe" data-toggle="tab"><img src=<?php echo $root_url . "/images/kauwe.jpg"?> class="bio_pic"></a></li>
            </ul>
      
            <div class="tab-content">
              <div class="tab-pane active" id="oddo">
                <h4 class="center">Salvatore Oddo</h4>
                <h5 class="center">University of Texas Health Science Center at San Antonio<br />Department of Physiology</h5>
                <div class="divider_red"></div>
                <p class="tab_text">After earning his PhD in Neurobiology and Behavior and post-graduate work at the University of California, Irvine, Salvatore moved to the University of Texas Health Science Center at San Antonio as an Assistant Professor to continue researching the molecular and cellular basis of cognitive decline in Alzheimer’s disease (AD).   In particular, Salvatore’s laboratory uses broad genetic approaches to directly manipulate the mouse genome, allowing precise temporal control and regional specificity that allows direct mapping to the development stages of the central nervous system.  Ultimately, these studies are hoped to reveal the molecular features underlying motor and cognitive dysfunction in mammals.</p>
                <p class="tab_text">Salvo’s proposal is aimed at investigating a synaptic dysfunction.  Although synapse formation is known to be a key component of learning and memory, Salvo’s group has shown, preliminarily, that facilitating neuronal depolarization, or the ‘rising phase’ of an action potential that causes a neuron to fire, by providing mice with clozapine-N-oxide (CNO), a pharmacological compound that binds to hM3Dq receptors increased the mice’s performance on a memory-based learning maze task compared to controls.  Following these results, Salvo and Mark’s proposal aims to further characterize the molecular mechanism of these findings to determine if facilitating neuronal synaptic activity could be a therapeutic treatment for AD.</p>
              </div>
        
              <div class="tab-pane" id="meadow">
                <h4 class="center">Mark Meadowcroft</h4>
                <h5 class="center">Pennsylvania State University College of Medicine<br />Departments of Neurosurgery and Radiology</h5>
                <div class="divider_red"></div>
                <p class="tab_text">Following his PhD and a post-doctoral position at Penn State University College of Medicine, Mark undertook an industrial appointment at Bristol-Myers Squibb during which he was involved in both preclinical and clinical work involving MR neuro-imaging before returning to Penn State as an Assistant Professor. Mark’s research has built upon his time in academia and industry and focuses on using magnetic resonance (MR) imaging to understand the underlying mechanism and histo-pathological basis of beta amyloid (Aβ) plaque formation and neural tissue alterations in Alzheimer’s disease (AD).  In particular, Mark’s research has melded MR visualization with histological evaluation, such as immune-fluorescent labeling of amyloid plaques, neurons and oligodendrocytes, iron staining or labeling of neuronal and oligodendroctye toxicity, to better understand and quantify what is observed in imaging and cell-based studies.</p>
               <p class="tab_text">Mark’s proposal seeks to link the histological cyto-architectural causes of MR-related changes in white matter (WM) in AD.  In particular, his research will look at the changes that occur to intra- and subcortical myelin during early and late stages of AD progression.  With MR imaging, Mark hopes to identify a quantifiable difference between early and late myelination in AD.  Additionally, microstructural changes to the cyto-architecture in these regions will be quanitified with immunohistological techniques.  By tying together these histological stains with histological MR imaging, this research will provide a more unified understanding of the cellular changes that occur in AD and other neurological diseases.</p>
              </div>
        
              <div class="tab-pane" id="kim">
                <h4 class="center">Jungsu Kim</h4>
                <h5 class="center">Washington University in St. Louis<br />Department of Neurology</h5>
                <div class="divider_red"></div>
                <p class="tab_text">Jungsu Kim completed his PhD in neurology at the Mayo Clinic before moving to Washington University School of Medicine, where he is now an Assistant Professor in the Department of Neurology.  During his graduate work, Jungsu developed a novel method for making somatic brain transgenic mice which he is continued to apply in gene knockdown experiments to study small, regulatory sequences of RNA called microRNA (miRNA). </p> 
                <p class="tab_text">Jungsu’s proposal brings in Salvatore Oddo, and builds upon Jungsu’s previous research which identified a cluster of six miRNAs (miR-17-92) that are expressed in both mouse and human and are significantly downregulated in the temporal complex of Alzheimer’s disease (AD) patients.  The miR-17-92 cluster was also shown to suppress expression of both APP and the phosphatase and tensin homolog (PTEN), suggesting that miR-17-92 could contribute to AD pathogenesis by regulating both Aβ and p-tau, whose accumulation is a known factor in AD pathogenesis, via APP and PTEN.  The first portion of Jungsu’s proposal aims to experimentally confirm several transcription factors predicted using a bioinformatics approach that are likely to regulate the expression of the miR-17-92 cluster.  In the second stage, the effect of miR-17-92 overexpression on p-tau levels in the brain will be examined.  With positive results, miR-17-92 modulators could represent a novel class of AD therapeutics.</p>
          
              </div>
        
              <div class="tab-pane" id="kauwe">
                <h4 class="center">John Kauwe</h4>
                <h5 class="center">Brigham Young University<br />Departmetns of Biology and Neurosciences</h5>
                <div class="divider_red"></div>
                <p class="tab_text">While pursuing his PhD at Washing University in St. Louis, John Kauwe developed a strong skillset in population and statistical genetics that has led him into a research career based on employing a variety of genetic approaches to understand the genetics of Alzheimer’s disease (AD).  In his post-graduate work, John has investigated both gene-gene and gene-environment interactions in AD.  As a Research Associate at the Departments of Biology and Neuroscience at Brigham Young University, John has used whole genome and whole exome sequence data to identify variants causing a loss of function of the TREM2 gene that have been shown to strongly influence the risk of AD and is currently funded to conduct genome-wide association studies to learn more about the rate of progression of AD and other effects of the disease.</p>
                <p class="tab_text">John’s research proposal capitalizes upon his work in large-scale data analysis with the aim of identifying new biomarkers for preclinical AD.  As a progressive disease, AD has been shown to have three stages marked by (stage 1) accumulation of indicators of brain amyloidosis, (stage 2) neurodegeneration and (stage 3) subtle cognitive decline.  Identifying specific imaging markers for these stages, in particular the stage 3, is essential, as they can provide a window for early therapeutic or preventative intervention.  Several major pathological features for AD marking the progression of the disease have been identified, most notably the accumulation of beta amyloid (Aβ) at the early stages of AD, which can be seen with PET-based Aβ binding ligands, and atrophy at the late stages, which can be seen with structural MRI.  While AD is typically considered a gray matter (GM) disease involving the neuronal cell bodies, dendrites, unmyelinated axons and glial cells, recent studies have also provided evidence for changes in white matter (WM), including decreased myelin and axonal density, loss of oligodendrocytes and activation of glial cells, suggesting that WM could also serve as an indicator of AD progression.  John’s research will aim to investigate the involvement of WM in AD progression and its significance in the stages between Aβ accumulation and atrophy.  Establishment of a complementary relationship between WM and the existing imaging biomarkers of AD will allow WM to be used as an early biomarker for preclinical diagnosis of AD.</p>
              </div>
            </div>
          </div>
      
            <h5 class="center">Find out more about the participants in CADD 2013: </h5>
            <a href=<?php echo $root_url . "/ad/2013/ji13.php"?>><img src=<?php echo $root_url . "/images/ji13image.png"?>  class="link_btn" style="width: 75%; margin-left: 12%"></a>
            
          <div style="padding-top: 230px; padding-bottom: 40px;">
            
            <h5>Collaboration</h5>
            <p class="main_text" style="margin-bottom: 40px;">While the main goal of the conference was to encourage young researchers to come up with novel ideas and ultimately fund the best, an additional major aim was to bring together this up-and-coming generation of AD researchers in one environment, where they were king.  With 15 JI’s spanning a range of research areas from genetic studies to clinical initiatives to neuroimaging, the range of interests was extensive and the potential for collaboration rife.  Whether or not they collaborated with each other formally to write their proposals, the JI’s were encouraged throughout the weekend to get to know their colleagues as peers both in academic research and socially.  With the focus of the conference on the JI’s, they were given the opportunity to establish themselves as the next generation of key players in the AD research field.</p>
           
          </div>
        </div>
      </div>
    </div>
  


</body>
</html>