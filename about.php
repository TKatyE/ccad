<?php 
  
  $ini_array = parse_ini_file("config.ini");
  $root_url = $ini_array["root_url"];
  $image_url = $ini_array["image_url"];
  
  include $root_url . 'head.html';
  include $root_url . 'navbar.html';
  
  
?>


<body>
  <div class="container-fluid no_header">
    <div class="row-fluid">
      <div class="span12">
        <h1 class="center">About the Charleston Conference Series <br />and the New Vision Awards</h1>
      </div>
    </div>
  </div>
  
  <div class="container-fluid">
    <div class="divider_red"></div>
    
    <div class="row-fluid">
      <div class="span12">
        <p>Founded in 2012-2013, the Charleston Conference series was initiated by philanthropist Carole L. Pittelman, who recognized that accelerating novel Alzheimer's disease (AD) research can address the pressing public health need for rapid advances in understanding and treating AD.</p>
        
        <div class="img_left">
          <img src=<?php echo $root_url . "/images/carole_leonard2.png" ?> class="img_left">
          <div class="img_caption"><p class="img_caption">Leonard Litwin (center) with daughter Carole Pittelman (right).</p></div>
        </div>
        

         <p>To this end, the Charleston Conference on Alzheimer's disease (CCAD) series will expedite innovative thinking by providing a funding mechanism that will assist rising junior investigators by encouraging them to engage in unusual, high-risk and promising research that pushes the frontiers of AD research and to develop collaborations with fellow innovative thinkers to facilitate high-impact projects.</p>
         
        <p>The CCAD series is committed to providing a unique alternative to conducting science by prioritizing novelty and collaboration in funding decisions.  By encouraging its participants to propose ideas that are 'outside-of-the-box' and push the boundaries of AD research, the conference aspires to be a model for future mechanisms and changes in funding trends, while providing a venue in which junior investigators can be recognized for nascent ideas that, if provided with seed funding, can grow into a promising career of innovation with the cooperation of like-minded investigators.</p>
          
        <p>The projects funded by the New Vision Awards are expected to change the trajectory of current AD research, which will better serve the millions of individuals whose lives are affected by AD.</p>
        
        <p>Additionally, the Charleston Conference series is a unique networking opportunity for mid-level researchers in the field of Alzheimer's disease.  While scientific conferences focusing on specific research areas are common, few emphasize and focus solely on researchers at the mid-stages of their career.  By bringing together junior researchers across the AD research field and giving them the opportunity to meet their colleagues, CCAD encourages the researchers to establish a network for collaboration early on.  As the junior investigators move up in their field, they will have an already strong network of scientific peers that will ultimately strengthen progress in AD research.</p>
        
        <p>To facilitate the growth of this network, the CCAD series has established a <a href="http://sandyseas.com/test32819/forum3412/">discussion forum</a>, <a href="/blog.php">blog</a> and social media outlets for the CCAD participants and others involved in AD research and those interested in AD to stay connected.</p> 
        
        <div class="divider_red" style="margin-top: 40px; margin-bottom: 30px;"></div>
      </div>
    </div>
    
    <div class="row-fluid">
        <div class="span12">
          <div class="span5" style="margin-top: 80px;">
            <br /><br />
            <h5>To hear more about the CCAD Conference Series and the issues it seeks to address from the <a href="/ad/2014/committee14.php">Nomination Committee</a>, check out the videos at right.</h5>
            <br /><br />
            <h5>To find out more about how CCAD 2013 went, click <a href="/ad/2013/ccad2013.php">here</a> for the recap.</h5>
            <br /><br />
            <h5>For information about this year's conference, check out the <a href="/ad/2014/ccad2014.php">CCAD 2014 information page</a>.</h5>
            <br /><br />
          </div>
          
          <div class="span7">
              <ul class="nav nav-pills" style="margin-left: 100px; width: 100%">
                <li class="active" style="margin-left: 30px;"><a href="#ben" data-toggle="tab"><img src="/images/wolozin_small.jpg" class="bio_pic_sm"></a></li>
                <li><a href="#joe" data-toggle="tab"><img src=<?php echo $root_url . "/images/joehead.png"?>  class="bio_pic_sm"></a></li>
                <li><a href="#karen" data-toggle="tab"><img src=<?php echo $root_url . "/images/Duff_small.jpg"?>  class="bio_pic_sm"></a></li>
                <li><a href="#alison" data-toggle="tab"><img src=<?php echo $root_url . "/images/Goate_small.jpg"?>  class="bio_pic_sm"></a></li>
                <li><a href="#peter" data-toggle="tab"><img src=<?php echo $root_url . "/images/Davies_small.jpg"?> class="bio_pic_sm"></a></li>
              
              </ul>

              <div class="tab-content" style="float: left; margin-left: 60px; padding-left: 40px; padding-top: 40px; padding-bottom: 40px;">
                <div class="tab-pane active" id="ben">
                  <video width="540px" height="auto" controls>
                    <source src=<?php echo $root_url . "/images/benvideo.mp4"?> type="video/mp4">
                    <object data=<?php echo $root_url . "/images/benvideo.mp4"?> width="320" height="240">
                      <embed src=<?php echo $root_url . "/images/benvideo.swf"?> width="320" height="240">
                    </object> 
                  </video>
                </div>
                
                <div class="tab-pane" id="joe">
                  <video width="540px" height="auto" controls>
                    <source src=<?php echo $root_url . "/images/joevideo.mp4"?> type="video/mp4">
                    <object data=<?php echo $root_url . "/images/joevideo.mp4"?> width="320" height="240">
                      <embed src=<?php echo $root_url . "/images/joevideo.swf"?> width="320" height="240">
                    </object> 
                  </video>
                </div>
                
                <div class="tab-pane" id="alison">
                  <video width="540px" height="auto" controls>
                    <source src=<?php echo $root_url . "/images/alisonvideo.mp4"?> type="video/mp4">
                    <object data=<?php echo $root_url . "/images/alisonvideo.mp4"?> width="320" height="240">
                      <embed src=<?php echo $root_url . "/images/alisonvideo.swf"?> width="320" height="240">
                    </object> 
                  </video>
                </div>
                
                <div class="tab-pane" id="karen">
                  <video width="540px" height="auto" controls>
                    <source src=<?php echo $root_url . "/images/karenvideo.mp4"?> type="video/mp4">
                    <object data=<?php echo $root_url . "/images/karenvideo.mp4"?> width="320" height="240">
                      <embed src=<?php echo $root_url . "/images/karenvideo.swf"?> width="320" height="240">
                    </object> 
                  </video>
                </div>
                
                <div class="tab-pane" id="peter">
                  <video width="540px" height="auto" controls>
                    <source src=<?php echo $root_url . "/images/petervideo.mp4"?> type="video/mp4">
                    <object data=<?php echo $root_url . "/images/petervideo.mp4"?> width="320" height="240">
                      <embed src=<?php echo $root_url . "/images/petervideo.swf"?> width="320" height="240">
                    </object> 
                  </video>
                </div>
              </div>
          </div>
        </div>
    </div>

    <div class="row-fluid">
      <div class="span12">
    
      <div class="divider_red" style="margin-top: 30px;"></div>
      
      <h4 class="center">Questions or comments about CCAD?  <a href=<?php echo $root_url . "/contact.php"?>>Contact Us</a> here.</h4>
      <div class="divider_red" style="margin-top: 30px;"></div>
    </div>
  </div>
    
    
    
  </div>
</body>
</html>

