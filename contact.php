<?php
$ini_array = parse_ini_file("config.ini");
$root_url = $ini_array["root_url"];

include $root_url . 'head.html';
include $root_url . 'navbar.html';

?>


<body>
  <div class="container main_page" style="margin-top: 140px;">
    <h1 class="center header_main">Questions? Comments?</h1>
    <h2 class="center" style="margin-top: -40px; margin-bottom: 40px;">Contact CCAD</h2>
    <h5 class="center">Alternatively, you can email us at <a href="mailto:ccad@musc.edu">ccad@musc.edu</a>.</h5>
  </div>
  <div class="container">
<form class="form-horizontal tan_box" name="contactform" method="post" action="send_email_form.php">
  <div class="control-group">
    <label class="control-label" for="first_name">First Name</label>
    <div class="controls">
      <input type="text" class="input-large" name="first_name">
    </div>
  </div>
  
  <div class="control-group">
    <label class="control-label" for "last_name">Last Name</label>
    <div class="controls">
      <input type="text" class="input-large" name="last_name">
    </div>
  </div>
  
  <div class="control-group">
    <label class="control-label" for "email">Email Address</label>
    <div class="controls">
      <input type="text" class="input-large" name="email">
    </div>
  </div>

  
  <div class="control-group">
    <label class="control-label" for "message">Message</label>
    <div class="controls">
      <textarea rows="8" name="message"></textarea>
    </div>
  
  <div class="span2" style="margin-top: 40px; margin-bottom: -20px; margin-left: 160px;">
    <button type="submit" class="btn btn-danger btn-large" value="submit">Send</button>
  </div>
  </div>
  
  
</form>
</div>
  
  