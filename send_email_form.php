<?php

  include "/head.html";
  include "/navbar.html";
  
?>
  <body>
    <div class="container" style="margin-top: 300px; margin-left: 28%;">
<?php


  if(isset($_POST['email'])) {
    $email_to = "tara@eckenrode.com";
    $email_subject = "CCAD Website Message";
    
    function died($error) {
      echo "We are sorry, but your message couldn't send.<br />";
      echo "Errors: " . $error . "<br /><br />";
      echo "Please fix these errors and try to send your message again.";
      die();
    }
    
    if(!isset($_POST['first_name']) ||
      !isset($_POST['last_name']) ||
      !isset($_POST['email']) || 
      !isset($_POST['message'])) {
        died("We are sorry, but there appears to be a problem with your message.");
    }
    
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
      if(!preg_match($email_exp,$email)) {
        $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
      }
        $string_exp = "/^[A-Za-z .'-]+$/";
      if(!preg_match($string_exp,$first_name)) {
        $error_message .= 'The First Name you entered does not appear to be valid.<br />';
      }
      if(!preg_match($string_exp,$last_name)) {
        $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
      }
      if(strlen($message) < 2) {
        $error_message .= 'The Comments you entered do not appear to be valid.<br />';
      }
      if(strlen($error_message) > 0) {
        died($error_message);
      }
        $email_message = "Form details below.\n\n";

        function clean_string($string) {
          $bad = array("content-type","bcc:","to:","cc:","href");
          return str_replace($bad,"",$string);
        }

        $email_message .= "First Name: ".clean_string($first_name)."\r\n";
        $email_message .= "Last Name: ".clean_string($last_name)."\r\n";
        $email_message .= "Email: ".clean_string($email)."\r\n";
        $email_message .= "Message: ".clean_string($message)."\r\n";
        
    $headers = "From: " . $email . "\r\n" . "Reply-To: eckenrod@musc.edu" . "\r\n" . "X-Mailer: PHP/" . phpversion();
    mail($email_to, $email_subject, $email_message, $headers);
    ?>
    Your message has been sent!
    Thank you for contacting the CCAD team.  We will be in touch as soon as possible.
    </div>
  </body>
</html>
<?php
  }
?>
