<?php
$ini_array = parse_ini_file("config.ini");
$root_url = $ini_array["root_url"];
$image_url = $ini_array["image_url"];

include $root_url . 'head.html';
include $root_url . 'navbar.html';

?>

<body>
  <div class="container-fluid no_header">
    <div class="row-fluid">
      <div class="span12">
        <h1 class="center">About Alzheimer's Disease</h1>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="divider_red"></div>
    
    <div class="row-fluid">
      <div class="span12">
        <div class="span3">
          <div class="img_left">
            <img src=<?php echo $image_url . "/images/AD_PlaquesTangles.jpg" ?> class="img_left">
            <div class="img_caption" style="width: auto;"><p class="img_caption" style="width: 250px;">Image copyright Learn Genetics from the University of Utah and is available <a href="http://learn.genetics.utah.edu/content/disorders/whataregd/alzheimers/">here</a>.</p></div>
          </div>
        </div>
        <div class="span9">
      
          <p>Alzheimer’s Disease (AD) is the most common form of dementia, accounting for 50 to 80 percent of cases.  While the majority of AD patients are over age 65, an estimated 5% of cases of ‘early-onset’ AD occur in individuals as young as 30 to 40 years old.</p>
      
          <p>Alzheimer’s is a progressive disease, and is the sixth leading cause of death in the United States.  While treatments do exist to slow the advancement of the disease and help make the lives of patients and their carers easier, there is no cure.  On average, patients live for eight years following the appearance of noticeable symptoms and diagnosis.</p>
      

      
          <p>In patients in the early stages of AD, microscopic changes in the brain can be seen, and as the disease progresses, two main characteristics are observed: ‘plaques’ and ‘tangles’.  Plaques are formed by build ups of a small peptide called beta-amyloid that is a by-product of processing of one of a protein thought to be involved in neuronal development.  As these peptides accumulate, they are thought to disrupt communication between nerve cells in the brain.  Tangles, on the other hand, are formed by aggregations of a protein called tau that is involved in stabilizing the cytoskeleton of the cell.</p>
          <p>Although Alzheimer’s Disease patients show a consistent pattern of plaques and tangles, exact details of how both entities contribute to the development and progression of the disease.  Further understanding of AD on a detailed, molecular level will be crucial to developing treatments for the disease and ultimately, a cure.</p>
        </div> 
        
      </div>
    </div>
      
  </div>
</body>