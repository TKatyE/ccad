"""
0|id|INTEGER|1||1
1|user_id|integer|0||0
2|contact_id|integer|0||0
3|current_uni|varchar(255)|0||0
4|uni_department|varchar(255)|0||0
5|phd_md_uni|varchar(255)|0||0
6|research_level|varchar(255)|0||0
7|profile|varchar(255)|0||0
8|created_at|datetime|1||0
9|updated_at|datetime|1||0
"""

class JrResearch < ActiveRecord::Base
  attr_accessible :id, :user_id, :contact_id, :current_uni, :uni_department, :phd_md_uni, :research_level, :profile, :attachment

  belongs_to :user
  
  mount_uploader :attachment, ResumeUploader

   def remove_attachment!
     begin
       super
     rescue Fog::Storage::Rackspace::NotFound
     end
   end

   def remove_previously_stored_attachment
     begin
       super
     rescue Fog::Storage::Rackspace::NotFound
       @previous_model_for_attachment = nil
     end
   end
  

end
