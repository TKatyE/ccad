class User < ActiveRecord::Base
  
  attr_accessible :name, :password, :password_confirmation
  validates :name, presence: true, uniqueness: true
  has_secure_password
  
  
  has_one :contact
  has_one :jr_research
  has_many :interests, :through => :interest_users
  belongs_to :interest_user
  
  after_destroy :ensure_an_admin_remains
  
  def add_contact_info(user_id)
  
    # Add a contact entry for a new user.
    Contact.find_or_create_by_user_id(:user_id => user_id)

    # Add the research information for the user.
    JrResearch.find_or_create_by_user_id(:user_id => user_id)
    
    # Add the user_interests.
    InterestUser.find_or_create_by_user_id(:user_id => user_id)
  end
  
  def delete_references(user_id)
    # Delete the contact information
    @contact = Contact.find_by_user_id(user_id)
    @contact.destroy
    # Delete the jr_research information
    @jr_research = JrResearch.find_by_user_id(user_id)
    @jr_research.destroy
    # Delete the user_interest
    @interest_user = InterestUser.find_by_user_id(user_id)
    @interest_user.destroy
  end
  
  
  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end
  
  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end
  
  
  private
    def ensure_an_admin_remains
      if User.count.zero?
        raise "Can't delete last user"
      end
    end
    

  
  has_one :contact
  has_one :jr_research
  has_many :user_interest
  
end
