class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by_name(params[:name])
    if user and user.authenticate(params[:password])
      
      if params[:remember_me]
        cookies.permanent[:auth_token] = user.auth_token
      else
        cookies[:auth_token] = user.auth_token
      end
      
      session[:user_id] = user.id
      @user = session[:user_id]
      
      if @user == 1
        redirect_to user_admin_index_url(session[:user_id])
      else
        logger.info("#{@user}")
        redirect_to user_admin_url(@user)
      end
    else
      redirect_to login_url, alert: "Invalid user/password combination."
    end
  end

  def destroy
    session[:user_id] = nil
    cookies.delete(:auth_token)
    redirect_to login_url, notice: "Logged out"
  end
end
