class ApplicationController < ActionController::Base
  protect_from_forgery
  
  
  protected
  
    def authorize
      logger.info("ID #{session[:user_id]}")
      if session[:user_id] == nil
        redirect_to login_url, :notice => "Please log in."
      else
        unless User.find_by_id(session[:user_id])
          redirect_to login_url, :notice => "Please log in."
        end
    
    
        if User.find(session[:user_id]) != User.find(params[:user_id])
          redirect_to login_url, :notice => "Please log in."
        end
      end
    end
    
    
    def authorize_super
      if session[:user_id] == nil
        redirect_to login_url, :notice => "Please log in."
      end
      user = User.find_by_id(session[:user_id])
      unless session[:user_id] == 1
        logger.info("#{session[:user_id]} id not 1")
        redirect_to login_url, :notice => "Please log in."
        logger.info("redirecting")
      end
    end
    
  private
  def current_user
    @current_user ||= User.find_by_auth_token( cookies[:auth_token]) if cookies[:auth_token]
  end
  helper_method :current_user
end
