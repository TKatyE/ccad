class ContactsController < ApplicationController
  # GET /contacts
  # GET /contacts.json
  
  before_filter :authorize, :except => [:new, :create] 
  before_filter :authorize_super, :only => [:new, :create]
  before_filter :user_filter
  
  def index
    @contacts = Contact.all
    @contact = Contact.find_by_user_id(session[:user_id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contacts }
    end
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
    @contact = Contact.find(params[:id])
    @jr_research = JrResearch.find_by_user_id(@contact.user_id)
    @user_contact = Contact.find_by_user_id(params[:user_id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contact }
    end
  end

  # GET /contacts/new
  # GET /contacts/new.json
  def new
    @contact = Contact.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contact }
    end
  end

  # GET /contacts/1/edit
  def edit
    @contact = Contact.find(params[:id])
    if @contact.user_id != session[:user_id]
      redirect_to login_url, :notice => "Please log in."
    end
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(params[:contact])

    respond_to do |format|
      if @contact.save
        format.html { redirect_to @contact, notice: 'Contact was successfully created.' }
        format.json { render json: @contact, status: :created, location: @contact }
      else
        format.html { render action: "new" }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /contacts/1
  # PUT /contacts/1.json
  def update
    @contact = Contact.find(params[:id])
    @jr_research = JrResearch.find_by_user_id(@user.id)
    
    respond_to do |format|
      if @contact.update_attributes(params[:contact])
        format.html { redirect_to edit_user_jr_research_url(@user, @jr_research) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact = Contact.find(params[:id])
    @contact.destroy

    respond_to do |format|
      format.html { redirect_to contacts_url }
      format.json { head :no_content }
    end
  end
  
  protected 
  def user_filter
    @user = User.find(session[:user_id])
  end
  

end
