class InterestUsersController < ApplicationController
  # GET /interest_users
  # GET /interest_users.json
  
  skip_before_filter :authorize, :only => [:new, :create] 
  before_filter :authorize_super, :only => [:new, :create]
  before_filter :user_filter
  
  
  def index
    @interest_users = InterestUser.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @interest_users }
    end
  end

  # GET /interest_users/1
  # GET /interest_users/1.json
  def show
    @interest_user = InterestUser.find(params[:id])


    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @interest_user }
    end
  end

  # GET /interest_users/new
  # GET /interest_users/new.json
  def new
    @interest_user = InterestUser.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @interest_user }
    end
  end

  # GET /interest_users/1/edit
  def edit
    @interest_user = InterestUser.find(params[:id])
    @contact = Contact.find_by_user_id(session[:user_id])
    @selected = InterestUser.find(:all, :conditions => { :user_id => params[:user_id] })
  end

  # POST /interest_users
  # POST /interest_users.json
  def create
    @interest_user = InterestUser.new(params[:interest_user])

    respond_to do |format|
      if @interest_user.save
        format.html { redirect_to @interest_user, notice: 'User interest was successfully created.' }
        format.json { render json: @interest_user, status: :created, location: @interest_user }
      else
        format.html { render action: "new" }
        format.json { render json: @interest_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /interest_users/1
  # PUT /interest_users/1.json
  def update
    @all_interest_users = params[:inters]
    logger.info(" interest -- #{@all_interest_users}")
    @interest_users = @all_interest_users[:interest_id]
    logger.info("interest IDs -- #{@interest_users}")
    @interest_user = InterestUser.find(params[:id])
    
    @contact = Contact.find_by_user_id(session[:user_id])
    
    respond_to do |format|

      @interest_user.update_all(@interest_users, @contact)
      if @interest_user.update_attributes(params[:interest_user])
        format.html { redirect_to user_admin_url(@user), notice: 'Your profile has been successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @interest_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /interest_users/1
  # DELETE /interest_users/1.json
  def destroy
    @interest_user = InterestUser.find(params[:id])
    @interest_user.destroy

    respond_to do |format|
      format.html { redirect_to interest_users_url }
      format.json { head :no_content }
    end
  end
  
  protected
  def user_filter
    @user = User.find(session[:user_id])
  end
end
