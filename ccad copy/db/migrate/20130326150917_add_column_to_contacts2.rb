class AddColumnToContacts2 < ActiveRecord::Migration
  def change
    add_column :contacts, :title, :string
    add_column :contacts, :phd_md, :string
  end
end
