class AddAttachmentToJrResearchers < ActiveRecord::Migration
  def change
    add_column :jr_researches, :attachment, :avatar
  end
end
