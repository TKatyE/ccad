class CreateJrResearches < ActiveRecord::Migration
  def change
    create_table :jr_researches do |t|
      t.integer :user_id
      t.integer :contact_id
      t.string :current_uni
      t.string :uni_department
      t.string :phd_md_uni
      t.string :research_level
      t.string :profile
      t.timestamps
    end
  end
end
