require 'test_helper'

class JrResearchesControllerTest < ActionController::TestCase
  setup do
    @jr_research = jr_researches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:jr_researches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create jr_research" do
    assert_difference('JrResearch.count') do
      post :create, jr_research: {  }
    end

    assert_redirected_to jr_research_path(assigns(:jr_research))
  end

  test "should show jr_research" do
    get :show, id: @jr_research
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @jr_research
    assert_response :success
  end

  test "should update jr_research" do
    put :update, id: @jr_research, jr_research: {  }
    assert_redirected_to jr_research_path(assigns(:jr_research))
  end

  test "should destroy jr_research" do
    assert_difference('JrResearch.count', -1) do
      delete :destroy, id: @jr_research
    end

    assert_redirected_to jr_researches_path
  end
end
